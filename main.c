#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC
#include <stdio.h>
#include <stdlib.h>
#include <crtdbg.h>
#include "student.h"

double* get_grades(int n) {
	double* grades = malloc(sizeof(double) * (n+1));
	*grades = n;
	printf("enter %d grades: ", (int)*grades);
	for (int i = 1; i <= *grades; i++) {
		scanf_s("%lf", grades + i);
	}

	return grades;
}

void clear(char* filename) {
	FILE* fp = fopen(filename, "w");
	fclose(fp);
}

int main(void) {
	clear("log.txt");
	FILE* log = fopen("log.txt", "a");
	Student* root = NULL;

	int records, n_grades;
	printf("enter the number of records to the list: ");
	scanf_s("%d", &records);
	printf("enter the number of exams to prompt entering: ");
	scanf_s("%d", &n_grades);

	for (int i = 0; i < records; i++) {
		printf("record %d: \n", i + 1);
		add_student(&root, get_grades(n_grades), log);
	}

	filter(&root, log);

	dealloc(&root);

	fclose(log);
	_CrtDumpMemoryLeaks();
	return 0;
}