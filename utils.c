#include "student.h"
#include <stdio.h>
#include <stdlib.h>

double avg(double* arr) {
	double res = 0;
	for (int i = 1; i <= *arr; i++) {
		res += *(arr + i);
	}
	return res / *arr;
}

void add_student(Student** root, double* grades, FILE* log) {
	Student* new_student = malloc(sizeof(Student));
	if (new_student == NULL) exit(0);

	//if is empty
	new_student->grades = grades;
	if (*root == NULL) {
		new_student->next = NULL;
		*root = new_student;
		fprintf(log, "added new student to empty list;\n");
		return;
	}

	//first check
	if (avg((*root)->grades) < avg(grades)) {
		new_student->next = (*root);
		*root = new_student;
		fprintf(log, "pushed new student to the front;\n");
		return;
	}

	Student* curr = *root;
	while (curr->next != NULL) {
		if (avg(curr->next->grades) <= avg(grades)) break;
		curr = curr->next;
	}
	new_student->next = curr->next;
	curr->next = new_student;
	fprintf(log, "added student with avg = %.2f to the list after avg = %.2f;\n", avg(new_student->grades), avg(curr->grades));
}

void displayLL(Student** root, FILE* log) {
	Student* curr = *root;
	while (curr != NULL) {
		fprintf(log, "%p || size = %d: [", curr, (int) * (curr->grades));
		for (int i = 1; i <= (int) * (curr->grades); i++) {
			fprintf(log, "%.2f, ", *(curr->grades + i));
		}
		fprintf(log, "\b\b] avg = %.2f || %p\n", avg(curr->grades), curr->next);
		curr = curr->next;
	}
	fprintf(log, "___________________________________________________________________________\n");
}

void delete_student(Student** root, Student* to_delete, FILE* log) {
	if (root == NULL) {
		return;
	}

	if (*root == to_delete) {
		fprintf(log, "deleting %p with %.2f. it points to %p\n", *root, avg((*root)->grades), (*root)->next);
		*root = (*root)->next;
		free((*root)->grades);
		free(*root);
		return;
	}

	Student* curr = *root;
	while (curr->next != to_delete) curr = curr->next;
	curr->next = to_delete->next;
	free(to_delete->grades);
	free(to_delete);
}

int to_pass(Student* check) {
	int bad_marks = 0;
	double* grs = check->grades;
	for (int i = 1; i <= *grs; i++) {
		if (*(grs + i) <= 2.0) bad_marks++;
	}

	return bad_marks >= 2;
}

void filter(Student** root, FILE* log) {
	fprintf(log, "list before filtering: \n");
	displayLL(root, log);

	if (*root == NULL) return;

	Student* curr = *root;
	while (curr != NULL) {
		Student* nxt = curr->next;
		if (to_pass(curr)) {
			fprintf(log, "deleting avg %.2f;\n", avg(curr->grades));
			delete_student(root, curr, log);
		}

		if (curr->next == NULL) break;
		curr = nxt;
	}
	fprintf(log, "\nlist after filtering: \n");
	displayLL(root, log);
}

void dealloc(Student** root) {
	if (*root == NULL) return;
	dealloc(&((*root)->next));
	free((*root)->grades);
	free(*root);
	*root = NULL;
}