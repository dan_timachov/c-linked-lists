#pragma once
#include <stdio.h>

typedef struct student {
	double* grades;
	struct student* next;
} Student;

double avg(double*);

void add_student(Student**, double*, FILE*);

void displayLL(Student**, FILE*);

void delete_student(Student**, Student*, FILE*);

int to_pass(Student*);

void filter(Student**, FILE*);

void dealloc(Student**);